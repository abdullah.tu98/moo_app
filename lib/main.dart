import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sp = await SharedPreferences.getInstance();
  runApp(MyApp(
    sp: sp,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.sp});
  final SharedPreferences sp;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(sp: sp),
    );
  }
}

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  HomePage({super.key, required this.sp});
  SharedPreferences sp;
  int count = 0;
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    widget.count = widget.sp.getInt("count") ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        title: const Text("عداد الشوكولامو"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'بونجور لقد تناولت ${widget.count} كاسة شوكولامو',
            style: const TextStyle(fontSize: 40),
            textAlign: TextAlign.center,
          ),
          Image.asset('assets/moo.png'),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              setState(() {
                widget.count++;
                widget.sp.setInt("count", widget.count);
              });
              showDialog(
                context: context,
                builder: (context) => const AlertDialog(
                    title: Icon(
                      Icons.check,
                      size: 35,
                      color: Colors.green,
                    ),
                    content: Text(
                      "صحتين 😋",
                      style: TextStyle(fontSize: 35),
                      textAlign: TextAlign.center,
                    )),
              );
            },
            backgroundColor: Colors.brown,
            child: const Icon(Icons.add),
          ),
          const SizedBox(
            height: 8,
          ),
          FloatingActionButton(
            onPressed: () {
              setState(() {
                widget.count--;
                widget.sp.setInt("count", widget.count);
              });
              showDialog(
                context: context,
                builder: (context) => const AlertDialog(
                    title: Icon(
                      Icons.close,
                      size: 35,
                      color: Colors.red,
                    ),
                    content: Text(
                      "له له 🤣",
                      style: TextStyle(fontSize: 35),
                      textAlign: TextAlign.center,
                    )),
              );
            },
            backgroundColor: Colors.brown,
            child: const Icon(Icons.minimize),
          ),
        ],
      ),
    );
  }
}
